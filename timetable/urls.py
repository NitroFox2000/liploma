from django.urls import path
from timetable import views

urlpatterns = [
    path('test_data/', views.geterate_test_data, name="test_data"),
    path('admin/timetable/blok_timetable/add-real-data/', views.add_real_data, name="t_data"),
    path('admin/timetable/blok_timetable/test_data/', views.admin_geterate_test_data, name="admin_test_data"),
    path('admin/timetable/blok_timetable/gen-tt/', views.admin_geterate_test_data, ),
    path('admin/timetable/blok_timetable/clear/', views.delete_everything, name="del"),
    
    # path('admin/timetable/blok_timetable/createTimeTable/', views.CreateTimeTable.as_view(), 
    #      name="t_createTimeTable"),
    
    path('admin/timetable/blok_timetable/createTimeTable/', views.createTimeTable, 
         name="t_createTimeTable"),
    
    path('', views.main_page, name="home"),
]

# http://127.0.0.1:8000/admin/timetable/blok_timetable/add-real-data/