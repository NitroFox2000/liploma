from django.apps import AppConfig


class TimetableConfig(AppConfig):
    # default_auto_field = 'django.db.models.BigAutoField'
    name = 'timetable'



from suit.apps import DjangoSuitConfig

class SuitConfig(DjangoSuitConfig):
    layout = 'horizontal'