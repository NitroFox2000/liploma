from ..models import *

import random

ROOMS = Room.objects.all()
classes = nClass.objects.all()
subjects = Subjects.objects.all()
teachers = TeacherProfile.objects.all()
tr_cal = CourseLesson.objects.all()
teachers_subjects = Teacher_Subjects.objects.all()
time_bloks = Blok_time.objects.all()

sep = "================================="
rooms_count = ROOMS.count()
times_count = time_bloks.count()
rooms_ids = []

subj_spec = []
room_spec = []

CONDITIONS = list()

# Вспомогательная: сгруппировать iterable по count штук
def group(iterable, count):
    """ Группировка элементов последовательности по count элементов """
    return zip(*[iter(iterable)] * count)

def avialable_room(subject):
    """ Возвращает список допустимых кабинетов для предмета 
    
    Все специализации предмета
    По всем кабинетам
        Если спец. кабинета В спец. предмета:
            Кабинет к доступным
    Вернуть все доступные кабинеты
    """
    subj = Subjects.objects.get(pk=subject)
    subject_spec_list = []#set()
    rooms = []
    
    for item in subj.spec.all():
        subject_spec_list.append(item.pk)
    
    for room in ROOMS:
        rooms_spec_list = []#set()
        
        for item in room.spec.all():
            rooms_spec_list.append(item.pk)
            
        # print(f'\t rooms_spec_list = {rooms_spec_list} \t subject_spec_list {subject_spec_list}')
        
        if rooms_spec_list == subject_spec_list:
            rooms.append(room.pk)
            # print(f'\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\t\t\t room = {room}')
        # else:
        #     print(f'{rooms_spec_list} != {subject_spec_list}')
            
    if not rooms:
        for room in ROOMS:
            rooms.append(room.pk)
    return rooms